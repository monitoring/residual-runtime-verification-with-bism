# Plotting the Results

## Dependencies

We use [`R`](https://www.r-project.org/) to plot the results.

You can run the following to install dependencies if you have R already:

* ggplot2
* dplyr

```
sudo make dependencies
```

## Plotting the Graphs

You need to run the experiments before plotting  to generate the data files.

To plot build-time mode results:

```
make buildtime
```

To plot load-time mode results:

```
make loadtime
```


## Cleaning up

To remove all generated files.

```
make clean
```



