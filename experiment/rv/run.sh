start=1
end=25
sleep=3

bencharray=(avrora fop pmd sunflow)
 
for j in "${bencharray[@]}"
do
	for ((i=start; i<=end; i++))
		do

		 
				
			java -jar -noverify dacapo.jar $j  -c MyCallback  2>&1 | tee temp.txt >> log.txt
		 
			
			numb=$(grep   -E -e '[0-9]+\.[0-9]+ KB' -o temp.txt)
			numb=${numb%" KB"}
			echo "$j,RV,$numb" >> memory.csv

			numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
			numb=${numb%" msec"}
			echo "$j,RV,$numb" >> time.csv



		sleep $sleep
	done
	 
done
