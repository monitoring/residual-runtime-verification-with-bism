import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.corse.fast.bism.transformers.FastTransformer;

import java.util.Arrays;
import java.util.List;

public class UnsafeHasNextDynamic extends FastTransformer {



    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {


        if (mc.methodName.endsWith("next") && mc.methodOwner.contains("Iterator")) {

            StaticInvocation sti = new StaticInvocation(observer.classPath4, observer.methodName);
            sti.addParameter(2);
            invoke(sti);
        }


        if (mc.methodName.endsWith("hasNext") && mc.methodOwner.contains("Iterator")) {

            StaticInvocation sti = new StaticInvocation(observer.classPath4, observer.methodName);
            sti.addParameter(1);
            invoke(sti);
        }
    }
}
