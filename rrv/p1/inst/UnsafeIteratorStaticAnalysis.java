import inria.bism.core.BenchStats;
import inria.bism.core.RunArgs;
import inria.bism.transformers.Hidden;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.dynamiccontext.MethodDynamicContext;
import inria.bism.transformers.staticcontext.*;
import inria.corse.fast.bism.transformers.FastTransformer;
import automata.*;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.Type;
import inria.bism.transformers.StaticInvocation;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * This transformer analyses a target program statically to find violating patterns of
 * UnSafeIterator property (a collection should not be updated when an iterator associated with it
 * is being used). For each method, the cfg is converted into an automata and checked to find
 * possible violating execution paths to the UnSafeIterator property. This is done by checking the
 * emptiness of the language that represents the intersection of cfg automata and the property. When
 * paths of the cfg show no violations, these paths are excluded from instrumentation in the second
 * transformer that instruments for the runtime verification of the property.
 */

public class UnsafeIteratorStaticAnalysis extends FastTransformer {

    /**
     * Represents the alphabet of CfgAutomaton, these are events we are interested in. Creation of
     * an iterator (c), update on the associated collection (u), calling next on an iterator (n)
     */
    List<String> alphabet = Arrays.asList(new String[] {"c", "u", "n", "@"});

    /**
     * Automaton that represents the control flow graph of a method
     */
    Automaton cfgAutomaton;

    /**
     * Maps each state in the CfgAutomaton to a basic block id
     */
    HashMap<Integer, State> statesMap;

    /**
     * Intial state of CfgAutomaton
     */
    State initial;

    /**
     * Stores all instructions of interest, producing events (alphabet letter)
     */
    HashSet<AbstractInsnNode> shadowNodes = new HashSet<>();

    /**
     * Stores all instructions that may be producing unsafe instructions
     */
    HashSet<AbstractInsnNode> escapeNodes = new HashSet<>();

    boolean inNewClass;

    @Override
    public void onClassEnter(ClassContext c) {

        inNewClass = true;

        BenchStats.name = RunArgs.scope.toString();
        BenchStats.addClass(c.name);

    }

    /**
     * On method enter we construct the automata by traversing the cfg. Each block is state in the
     * automata and edges are transitions.
     *
     * @param m method static context
     * @param dc method dynamic context
     */

    boolean safe = true;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        statesMap = new HashMap<Integer, State>();
        BasicBlock entryBlock = m.getEntryBlock();

        // maps each block to an automata state
        for (BasicBlock b : m.getBasicBlocks()) {
            State s = addOrGetState(b, b == entryBlock);
            // map all transitions
            for (BasicBlock succ : b.getSuccessorBlocks()) {
                State to = addOrGetState(succ, succ == entryBlock);
                Transition t = new Transition("", to);
                s.addTransition(t);
            }
        }
        cfgAutomaton = new Automaton(initial);
        escapeNodes = new HashSet<>();
        shadowNodes = new HashSet<>();
        cexists = false;

        int params = m.getNumberOfArguments();
        safe = true;

        for (int i = 0; i < params; i++) {
            DynamicValue dv = dc.getMethodArgs(m, i + 1);
            if (dv != null)
                if (dv.descriptor.contains("java/util/List")
                        || dv.descriptor.contains("java/util/Iterator")) {
                    safe = false;
                    // System.out.println("NotSafe " + m.className + "." + m.name);
                }
        }

    }



    /**
     * Captures events of interest in a method and pushes them to cfg automata
     *
     * @param mc method call static context
     * @param dc method call dyunamic context
     */

    boolean cexists;

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        boolean generatesLetter = false;

        if (mc.methodName.contains("iterator") && mc.methodOwner.contains("List")) {
            addLabelAutomataState("c", mc.ins.node, mc.ins.basicBlock.id);
            DynamicValue dv = dc.getMethodResult(mc);
            generatesLetter = true;

            cexists = true;
        }

        List<String> updateMethods = Arrays.asList("add", "remove", "set", "removeAll", "sort",
                "retainAll", "addAll", "replaceAll", "clear");

        for (String um : updateMethods) {
            if (mc.methodName.endsWith(um) && mc.methodOwner.contains("List")) {
                addLabelAutomataState("u", mc.ins.node, mc.ins.basicBlock.id);
                generatesLetter = true;
                break;

            }
        }

        if (mc.methodName.endsWith("next") && mc.methodOwner.contains("Iterator")) {
            addLabelAutomataState("n", mc.ins.node, mc.ins.basicBlock.id);
            generatesLetter = true;

        }

        // // //suspect any method invocation
        // if (!generatesLetter && mc.ins.opcode != Opcodes.INVOKESPECIAL &&
        // !mc.methodOwner.contains("java") ) {
        // addLabelAutomataState("@", mc.ins.node, mc.ins.basicBlock.id);

        // }



        // suspect any method invocation
        if (!generatesLetter && mc.ins.opcode != Opcodes.INVOKESPECIAL
                && !mc.methodOwner.contains("java")) {

            Type[] argumentTypes = Type.getArgumentTypes(mc.methodnode.desc);
            boolean possibleEscape = false;
            for (int i = 0; i < argumentTypes.length; i++) {
                String tt = argumentTypes[i].toString();
                if (tt.length() != 1) { // not a primitive
                    if (!tt.equals("Ljava/lang/String;")) {
                        possibleEscape = true;
                    }
                }
            }

            if (possibleEscape) {
                addLabelAutomataState("@", mc.ins.node, mc.ins.basicBlock.id);
            }
        }
    }

    @Override
    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {

        if (ins.opcode == Opcodes.PUTFIELD || ins.opcode == Opcodes.PUTSTATIC) {

            FieldInsnNode fn = (FieldInsnNode) ins.node;

            if (fn.desc.startsWith("L") && !fn.desc.contains("Ljava/lang/String")) {
                // System.out.println(fn.desc);
                addLabelAutomataState("@", ins.node, ins.basicBlock.id);
            }

        }

        if (ins.opcode == Opcodes.ARETURN) {
            String returnType =
                    Type.getReturnType(ins.basicBlock.method.methodNode.desc).toString();
            returnType = returnType.replace("[", "").replace(";", "");
            if (returnType.startsWith("L"))
                returnType = returnType.replaceFirst("L", "");

            if (returnType.contains("java")) {
                if (returnType.contains("Iterator") || returnType.contains("List")) {
                    addLabelAutomataState("@", ins.node, ins.basicBlock.id);
                    // safe = false;
                }
            } else {
                addLabelAutomataState("@", ins.node, ins.basicBlock.id);
                // safe = false;
            }

        }
    }

    /**
     * Finds violating execution paths and mark "safe" path to be hidden for isntrumentation
     *
     * @param m method static context
     * @param dc method dynamic context
     */
    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        if (shadowNodes.size() == 0)
            return;

        // preparation step for the constructed cfgAutomaton
        AutomataOperations.toCfgAutomata(cfgAutomaton);

        // generate automaton for bad prefixes
        Automaton pattern = constructBadPrefixAutomaton();

        Stack<State> work = new Stack();
        work.push(cfgAutomaton.getInitialState());
        HashSet<State> visited = new HashSet();
        HashSet<AbstractInsnNode> unSafeNodes = new HashSet<>();

        while (!work.isEmpty()) {
            State q = work.pop();
            if (!visited.contains(q)) {

                visited.add(q);
                // if(!unSafeNodes.contains(q.getFirstLabelNode())) {
                cfgAutomaton.setInitialState(q);
                Automaton j = AutomataOperations.intersection(cfgAutomaton, pattern, alphabet);
                Set<State> accepting = j.getAcceptStates();
                if (!accepting.isEmpty()) {
                    // find all states that reach the final state (all paths that may violate the
                    // property)
                    for (State e : j.getCoReachableStates()) {

                        for (AbstractInsnNode sn : shadowNodes) {
                            if (e.shadows.contains(sn.hashCode() + "")) {
                                unSafeNodes.add(sn);
                            }
                        }
                    }
                }
                // }

                for (Transition t : q.getTransitions()) {
                    work.push(t.getDestination());
                }
            }
        }

        if (inNewClass) {
            // BenchStats.name = RunArgs.scope.toString();
            // BenchStats.addClass(m.className);\
            BenchStats.inScope++;
            inNewClass = false;
        }

        int allshadowscount = shadowNodes.size();

        // Keep only events that are in safe execution paths
        // realNodes.removeAll(Arrays.asList(nodes.toArray()));

        for (AbstractInsnNode n : unSafeNodes) {
            if (shadowNodes.contains(n)) {
                shadowNodes.remove(n);
            }
        }

        // Tell BISM to hide instructions in safe execution paths for following transformers
        if (safe)
            m.markInstructionsHidden(shadowNodes);

        if (allshadowscount != 0)
            BenchStats.addmethod(m.name, safe ? shadowNodes.size() : 0, allshadowscount);

    }

    /**
     * Creates a new state per basic block, checks if state is already created then retrieves that
     * state
     *
     * @param b associated basic block
     * @param entry if the basic block is an entry block
     * @return a new state if there is no state associated with the basic block, or gets the
     *         associated state
     */
    private State addOrGetState(BasicBlock b, boolean entry) {
        if (statesMap.containsKey(b.id))
            return statesMap.get(b.id);

        State s = new State(b.id, entry, true);
        if (entry)
            initial = s;
        statesMap.put(b.id, s);
        return s;
    }

    /**
     * @param n Event label
     * @param node represents the instructions that generated the event
     * @param blockId basic block id
     */
    private void addLabelAutomataState(String n, AbstractInsnNode node, int blockId) {

        statesMap.get(blockId).addLabel(n, node);

        if (n.equals("@")) {
            escapeNodes.add(node);
        } else {
            shadowNodes.add(node);
        }
    }

    /**
     * Constructs an automata that accepts the bad prefixes of UnSafeList iterator. Automata is
     * equivalent to the regular expression {cu+n}
     *
     * @return
     */
    private Automaton constructBadPrefixAutomaton() {

        State init = new State(true, false);

        State one = new State(false, false);
        State two = new State(false, false);

        State three = new State(false, true);
        State escape = new State(false, true);

        init.addTransition(new Transition("c", one));

        one.addTransition(new Transition("n", one));
        one.addTransition(new Transition("c", one));
        one.addTransition(new Transition("u", one));

        one.addTransition(new Transition("u", two));

        two.addTransition(new Transition("u", two));
        two.addTransition(new Transition("c", two));
        two.addTransition(new Transition("n", two));

        two.addTransition(new Transition("n", three));

        init.addTransition(new Transition("@", three));
        init.addTransition(new Transition("@", one));
        init.addTransition(new Transition("@", two));

        one.addTransition(new Transition("@", one));
        one.addTransition(new Transition("@", three));
        one.addTransition(new Transition("@", two));

        two.addTransition(new Transition("@", two));
        two.addTransition(new Transition("@", three));

        //
        //
        // one.addTransition(new Transition("@", escape));
        // two.addTransition(new Transition("@", escape));

        return new Automaton(init);
    }

    public void dumpDot(String content, String fileName) throws IOException {
        FileWriter fileWriter = new FileWriter(fileName);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print(content);
        printWriter.close();
    }
}

