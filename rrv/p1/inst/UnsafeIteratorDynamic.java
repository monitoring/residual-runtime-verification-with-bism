import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.corse.fast.bism.transformers.FastTransformer;

import java.util.Arrays;
import java.util.List;

public class UnsafeIteratorDynamic extends FastTransformer {

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {


        List<String> updateMethods = Arrays.asList("add", "remove", "set", "removeAll", "sort",
                "retainAll", "addAll", "replaceAll", "clear");

        for (String um : updateMethods) {
            if (mc.methodName.endsWith(um) && mc.methodOwner.contains("List")) {

                StaticInvocation sti =
                        new StaticInvocation(observer.classPath4, observer.methodName);
                sti.addParameter(1);
                invoke(sti);


                break;

            }
        }
    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (mc.methodName.endsWith("iterator") && mc.methodOwner.contains("List")) {

            StaticInvocation sti = new StaticInvocation(observer.classPath4, observer.methodName);
            sti.addParameter(2);


            invoke(sti);

        }

        if (mc.methodName.endsWith("next") && mc.methodOwner.contains("Iterator")) {

            StaticInvocation sti = new StaticInvocation(observer.classPath4, observer.methodName);
            sti.addParameter(3);

            invoke(sti);
        }
    }
}
