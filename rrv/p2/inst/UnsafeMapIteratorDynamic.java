import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.corse.fast.bism.transformers.FastTransformer;

import java.util.Arrays;
import java.util.List;

public class UnsafeMapIteratorDynamic extends FastTransformer {

    List<String> updateMethods =
            Arrays.asList("merge", "compute", "computeIfPresent", "computeIfAbsent", "replace",
                    "remove", "putIfAbsent", "replaceAll", "put", "clear", "putAll", "setValue");


    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        for (String um : updateMethods) {
            if (mc.methodName.endsWith(um) && mc.methodOwner.contains("Map")) {

                StaticInvocation sti =
                        new StaticInvocation(observer.classPath4, observer.methodName);
                sti.addParameter(1);
                invoke(sti);

                break;
            }
        }

    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (mc.methodName.endsWith("iterator") && mc.methodOwner.contains("Collection")) {

            StaticInvocation sti = new StaticInvocation(observer.classPath4, observer.methodName);
            sti.addParameter(2);


            invoke(sti);
        }

        if (mc.methodName.endsWith("iterator") && mc.methodOwner.contains("Set")) {

            StaticInvocation sti = new StaticInvocation(observer.classPath4, observer.methodName);
            sti.addParameter(2);

            invoke(sti);
        }

        if (mc.methodName.endsWith("next") && mc.methodOwner.contains("Iterator")) {

            StaticInvocation sti = new StaticInvocation(observer.classPath4, observer.methodName);
            sti.addParameter(3);
            invoke(sti);
        }
    }
}
