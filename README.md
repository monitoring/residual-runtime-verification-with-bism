# Residual Runtime Verification via Reachability Analysis

## Implementation details

The work is implemented as a plugin (1 KLOC) to the [BISM](https://gitlab.inria.fr/bism/bism-public) Java byte-code instrumentation tool.
BISM is a lightweight instrumentation tool that features an expressive and high-level instrumentation language. 
Instrumentation directives in BISM are given with the so-called transformers, which resemble aspects of aspect-oriented programming.
Writing static analyzers requires a fine level of granularity when capturing program events.
The BISM instrumentation language provides access to the level of bytecode instructions with full static and dynamic context objects.
It is also control-flow aware.
That is, it generates CFGs for all methods and provides access to them in its language. %
Both features are essential for our residual analysis; this makes BISM the instrumentation framework of choice for our purposes. 
 
Moreover, BISM provides a mechanism to compose multiple transformers.
Transformers, in composition, are capable of controlling the visibility of instructions.
For each property, we then write two transformers: the first one, the static analyzer, that performs the residual analysis and hides safe instructions, and the second one is to instrument the residual part of the program for runtime monitoring.
We extend BISM with a module that provides automata operations.
The module is used to generate the CFG automata of methods and prepare the automaton for bad prefixes.
The module also allows us to detect the property-violating execution paths by intersecting the language of the CFG automata with the language of bad/good prefixes of the property under consideration.



## To run the tool

**/rrv** directory contains the code and tool needed to generate the instrumented classes.

Directories  **/p1**, **/p2**, and **/p3** contain the code for running RRV with P1, P2, and P3 from the paper espectively.

Navigate to each folder, and run the tool. You need **ant** installed on your machine. 

```
cd [chosen property]
ant build run
```

The tool will run by default on avrora, to change the benchmark open the *build.xml* from the property directory and change the *driver* value to the benchmark name.


To check the instrumented files, navigate to :

```
cd [chosen property]/out/instrumentation/instrumented

```

To clean the directory:

```
ant clean

```

## To run the experiment

**/experiment** directory contains the pre-packaged dacapo jars for each approach where the relevant classes have been replaced with the instrumented classes inside their respective jars (for both *rv* and *rrv* approaches).

To run the experiments

```
cd /experiment
make run
```

After the run to plot the charts (you need **R**):

```
cd /plot
make buildtime
```

In case **R** is not installed, to install it:

```
cd /plot
make dependencies
```

